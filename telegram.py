# encoding: utf-8

import warnings
import os
import os.path
import sys
import logging
import time
import signal
import json
import funcs

from logging.handlers import RotatingFileHandler

from config import *

warnings.filterwarnings("ignore")

LOG_FILE = '/var/log/%s-telegram.log' % (ALIAS)

RUN = True

def handler_stop_signals(signum, frame):
  global RUN
  RUN = False

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)

def messanger():

  logger = logging.getLogger(ALIAS)
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
  logger.setLevel(logging.INFO)
  handler = RotatingFileHandler(LOG_FILE, maxBytes=200000, backupCount=5)
  handler.setFormatter(formatter)
  logger.addHandler(handler)

  pid = "%s" % (os.getpid())

  logger.info('Start %s with Pid %s' % (ALIAS, pid))


  while RUN:

    #TELEGRAM

    recs_success = []
    recs_error = []

    telegram_mess_counter = 0
    rchat = []

    viva = funcs.viva(
      logger,
      {'data' : {'action' : 'user_get_message', 'params' : {'cod': COD_TRANSPORT, 'type' : TYPE_MESSANGER, 'limit': telegram_limit }}}
      )

    if viva['result'] == 'success':
      for m in viva['data']['messages']:

        if not m['id'] in rchat:

          rchat.append(m['id'])

          logger.info('Send message by Telegram: ID:%s' %  (m['id']))

          telegram = funcs.telegram(logger,m)
          telegram_mess_counter +=1
          if telegram_mess_counter > telegram_mess_per_second:
            telegram_mess_counter = 0
            time.sleep(1)
            rchat = []

          if telegram['result'] == 'success':
            recs_success.append(m['rec'])
          else:
            recs_error.append(m['rec'])


    #CONFIRM RECS
    recs = len(recs_success) + len(recs_error) 
    if recs > 0:
      data = funcs.viva(
        logger,
        {'data' : {'action' : 'user_set_message', 'params' : {'cod': COD_TRANSPORT, 'type' : TYPE_MESSANGER, 'recs' : {'success': recs_success, 'error': recs_error}}}}
        )


    time.sleep(5)

  logger.info('Stop %s with Pid %s' % (ALIAS, pid))



if __name__ == '__main__':
  messanger()
