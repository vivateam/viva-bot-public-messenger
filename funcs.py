# -*- coding: utf-8 -*-

import warnings
import json
import io
import os
import os.path
import sys
import requests
import logging
import time
import ssl
import hashlib

from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from requests.auth import HTTPBasicAuth
from config import *


def has_html_markup(text):
  try:
    soup = BeautifulSoup(text, 'html.parser')
    return len(soup.find_all()) > 0
  except Exception as e:
    return False


def cast_subscrstatusid(subscrstatusid):
  if subscrstatusid == 0:
    return 1
  else:
    return 0


def viva(logger, data):
  try:
    auth = HTTPBasicAuth(viva_username, viva_password)
    r = requests.post(viva_url, json=data, auth=auth, verify=viva_cert, headers=headers)
    if r.json:
      return r.json()
    else:
      logger.info('Error Vivadata')
      return {'result':'error'}
  except:
    logger.info('Error Vivadata')
    return {'result':'error'}


def telegram(logger, data):

  try:

    if has_html_markup(data) == True:
      stext = 'https://api.telegram.org/bot%s/sendMessage?chat_id=%s&parse_mode=HTML&text=%s' % (TOKEN, data['id'], data['message'] )
    else:
      stext = 'https://api.telegram.org/bot%s/sendMessage?chat_id=%s&parse_mode=Markdown&text=%s' % (TOKEN, data['id'], data['message'] )
    r = requests.get(stext)
    if r.json:
      stext = r.json()
      if stext['ok'] == True:
        return {'result':'success'}
      else:
        logger.info('Telegram answer some error: %s' % (stext))
        return {'result':'error'}
    else:
      logger.info('Error Telegram')
      return {'result':'error'}
    #return {'result':'success'}

  except:
    logger.info('Error Telegram')
    return {'result':'error'}
