# VIVA bot messenger

## Installation Telegram service

Create file /etc/systemd/system/viva_bot_messenger_[VIVA_ID]_telegram.service

```bash
[Unit]
Description=VIVA bot massenger for Telegram service
After=multi-user.target

[Service]
Type=simple
ExecStart=/var/lib/viva/viva_env/bin/python /var/lib/viva/bot_messenger_[VIVA_ID]/telegram.py

[Install]
WantedBy=multi-user.target
```

## Usage

```bash
systemctl enable viva_bot_messenger_[VIVA_ID]_telegram
systemctl start viva_bot_messenger_[VIVA_ID]_telegram
```